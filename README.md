Build:
```sh
./build.sh
```
Compile:
```sh
# compiles and produces bytecode
./fml.sh compile <input fml>
# compiles and produces debug printout of bytecode
./fml.sh -d compile <input fml>
```
Run:
```sh
# runs program from bytecode (heap size is set to maximum of u16)
./fml.sh run <input bc> --bc
# runs program from fml file with GC
./fml.sh run <input fml> --heap-size <size in MB>
# runs program from fml file with GC and produces csv log of allocations and garbage collecting
./fml.sh run <input fml> --heap-size <size in MB> --heap-log <csv log>
```