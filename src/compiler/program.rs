use crate::compiler::native::LeWrite;
use crate::compiler::program_object::ProgramObject;
use anyhow::*;
use std::io::Write;

type Index = u16;

pub struct Program {
    pub const_pool: Vec<ProgramObject>,
    pub globals: Vec<Index>,
    pub entry: Index,
}

impl Program {
    pub fn to_bytes<W: Write>(&self, output: &mut W) -> Result<()> {
        let const_size = self.const_pool.len() as u16;
        u16::le_write(const_size, output)?;
        self.const_pool
            .iter()
            .try_for_each(|po| po.to_bytes(output))?;

        let globals_size = self.globals.len() as u16;
        u16::le_write(globals_size, output)?;
        self.globals
            .iter()
            .try_for_each(|i| Index::le_write(*i, output))?;

        Index::le_write(self.entry, output)
    }
}
