use crate::compiler::native::LeWrite;
use anyhow::*;
use std::io::Write;

type Index = u16;

#[derive(Clone, Debug, PartialEq)]
pub enum Instruction {
    Label(Index),
    Literal(Index),
    Print { index: Index, args: u8 },
    Array,
    Object(Index),
    GetField(Index),
    SetField(Index),
    CallMethod { index: Index, args: u8 },
    CallFunction { index: Index, args: u8 },
    SetLocal(Index),
    GetLocal(Index),
    SetGlobal(Index),
    GetGlobal(Index),
    Branch(Index),
    Jump(Index),
    Return,
    Drop,
}

impl Instruction {
    pub fn to_bytes<W: Write>(&self, output: &mut W) -> Result<()> {
        match self {
            Instruction::Label(index) => {
                u8::le_write(0x00, output)?;
                u16::le_write(*index, output)
            }
            Instruction::Literal(index) => {
                u8::le_write(0x01, output)?;
                u16::le_write(*index, output)
            }
            Instruction::Print { index, args } => {
                u8::le_write(0x02, output)?;
                u16::le_write(*index, output)?;
                u8::le_write(*args, output)
            }
            Instruction::Array => u8::le_write(0x03, output),
            Instruction::Object(index) => {
                u8::le_write(0x04, output)?;
                u16::le_write(*index, output)
            }
            Instruction::GetField(index) => {
                u8::le_write(0x05, output)?;
                u16::le_write(*index, output)
            }
            Instruction::SetField(index) => {
                u8::le_write(0x06, output)?;
                u16::le_write(*index, output)
            }
            Instruction::CallMethod { index, args } => {
                u8::le_write(0x07, output)?;
                u16::le_write(*index, output)?;
                u8::le_write(*args, output)
            }
            Instruction::CallFunction { index, args } => {
                u8::le_write(0x08, output)?;
                u16::le_write(*index, output)?;
                u8::le_write(*args, output)
            }
            Instruction::SetLocal(index) => {
                u8::le_write(0x09, output)?;
                u16::le_write(*index, output)
            }
            Instruction::GetLocal(index) => {
                u8::le_write(0x0A, output)?;
                u16::le_write(*index, output)
            }
            Instruction::SetGlobal(index) => {
                u8::le_write(0x0B, output)?;
                u16::le_write(*index, output)
            }
            Instruction::GetGlobal(index) => {
                u8::le_write(0x0C, output)?;
                u16::le_write(*index, output)
            }
            Instruction::Branch(index) => {
                u8::le_write(0x0D, output)?;
                u16::le_write(*index, output)
            }
            Instruction::Jump(index) => {
                u8::le_write(0x0E, output)?;
                u16::le_write(*index, output)
            }
            Instruction::Return => u8::le_write(0x0F, output),
            Instruction::Drop => u8::le_write(0x10, output),
        }
    }
}
