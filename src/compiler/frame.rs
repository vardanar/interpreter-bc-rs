use anyhow::*;
use std::collections::HashMap;

use crate::parser::Identifier;

type Index = u16;

#[derive(Debug)]
pub struct Frame {
    pub scopes: Vec<HashMap<Identifier, Index>>,
    pub loc_n: u16,
}

impl Frame {
    pub fn new() -> Frame {
        let mut scopes = Vec::new();
        scopes.push(HashMap::new());
        Frame { scopes, loc_n: 0 }
    }

    //returns number of locals in first scope
    pub fn get_loc_n(&self) -> u16 {
        self.scopes.first().unwrap().len() as u16
    }

    pub fn alloc(&mut self, name: Identifier, ind: Index) -> Result<()> {
        //checking only most recent scope to allow variable shadowing
        if self.scopes.last().unwrap().contains_key(&name) {
            Err(anyhow!("redeclaration of variable '{}'", name))
        } else {
            let last = self.scopes.last_mut().unwrap();
            last.insert(name, ind);
            Ok(())
        }
    }

    pub fn seek(&self, name: &Identifier) -> Option<Index> {
        for scope in self.scopes.iter().rev() {
            match scope.get(name) {
                Some(ind) => return Some(*ind),
                None => (),
            }
        }
        None
    }
}
