use clap::{AppSettings, Clap};

pub mod compiler;
pub mod fml;
pub mod interpreter;
pub mod parser;
pub mod serializer;

use compiler::Compiler;
use interpreter::Machine;
use std::fs::File;
use std::io::{self, Read};

use fml::TopLevelParser;

#[derive(Clap)]
enum Command {
    Run {
        input: String,
        #[clap(short, long)]
        bc: bool,
        #[clap(long, default_value = "65534")]
        heap_size: u16,
        #[clap(long)]
        heap_log: Option<String>,
    },
    Compile {
        input: String,
    },
}

/// FML compiler written by fellow human beings Vašek and Narek
#[derive(Clap)]
#[clap(version = "1.0", author = "Narek Vardanjan", author = "Vašek Král")]
#[clap(setting = AppSettings::ColoredHelp)]
struct Opts {
    // prints debug output instead
    #[clap(short, long)]
    debug: bool,

    #[clap(subcommand)]
    cmd: Command,
}

fn debug_program(program: &serializer::Program) -> Result<(), anyhow::Error> {
    let mut pool_n = 0;
    println!("=== Const pool:");
    program.const_pool.iter().for_each(|i| {
        pool_n = pool_n + 1;
        println!("{}: {:?}", pool_n, i)
    });
    println!();

    println!("=== Globals:");
    program.globals.iter().for_each(|i| println!("{:?}", i));
    println!();

    println!("Entry point (index of entry method): {}", program.entry);
    Ok(())
}
fn main() -> Result<(), anyhow::Error> {
    let opts: Opts = Opts::parse();

    match opts.cmd {
        Command::Run {
            input,
            bc,
            heap_size,
            heap_log,
        } => {
            let mut file = Box::new(File::open(input).unwrap());
            if bc {
                let program = serializer::Program::from_bc(&mut file)?;
                let mut machine = Machine::new(program, io::stdout(), heap_size, heap_log);
                machine.run()
            } else {
                let mut s = String::new();
                file.read_to_string(&mut s).expect("read to string failed");

                let ast = TopLevelParser::new()
                    .parse(&s)
                    .expect("Error reading input");

                let program = Compiler::new().run(&ast)?;

                let mut b = Vec::new();
                program.to_bytes(&mut b)?;
                let bc = serializer::Program::from_bc(&mut b.as_slice())?;
                if opts.debug {
                    debug_program(&bc)?;
                };
                let mut machine = Machine::new(bc.clone(), io::stdout(), heap_size, heap_log);
                machine.run()
            }
        }
        Command::Compile { input } => {
            let mut file = Box::new(File::open(input).unwrap());
            let mut s = String::new();
            file.read_to_string(&mut s).expect("read to string failed");

            let ast = TopLevelParser::new()
                .parse(&s)
                .expect("Error reading input");

            let program = Compiler::new().run(&ast)?;

            if opts.debug {
                let mut pool_n = 0;
                println!("=== Const pool:");
                program.const_pool.iter().for_each(|i| {
                    pool_n = pool_n + 1;
                    println!("{}: {:?}", pool_n, i)
                });
                println!();

                println!("=== Globals:");
                program.globals.iter().for_each(|i| println!("{:?}", i));
                println!();

                println!("Entry point (index of entry method): {}", program.entry);
                Ok(())
            } else {
                program.to_bytes(&mut io::stdout())?;
                Ok(())
            }
        }
    }
}
