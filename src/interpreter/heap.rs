use std::collections::HashMap;

use anyhow::*;

use crate::serializer::instruction::Index;

use std::mem::{size_of, size_of_val};

#[derive(Clone, Debug, PartialEq, Eq, Hash, Copy)]
pub struct ObjectIndex(pub u64);

pub const NULL: ObjectIndex = ObjectIndex(0);

type HeapIndex = u64;

#[derive(Clone, Debug, PartialEq)]
pub enum RuntimeObject {
    Array(Vec<ObjectIndex>),
    Object {
        parent: ObjectIndex,
        fields: HashMap<Index, ObjectIndex>,
        methods: HashMap<Index, Index>,
    },
    Bool(bool),
    Integer(i32),
    Null,
}

impl From<bool> for RuntimeObject {
    fn from(b: bool) -> Self {
        RuntimeObject::Bool(b)
    }
}

impl From<i32> for RuntimeObject {
    fn from(i: i32) -> Self {
        RuntimeObject::Integer(i)
    }
}

#[derive(Clone)]
pub struct HeapObj {
    ro: RuntimeObject,
    mark: bool,
}

impl HeapObj {
    pub fn new(ro: RuntimeObject) -> HeapObj {
        HeapObj { ro, mark: false }
    }
}

pub struct VecHeap {
    byte_size: usize,
    obj_table: HashMap<ObjectIndex, HeapIndex>,
    obj_incr: u64,
    data: Vec<HeapObj>,
}

impl VecHeap {
    pub fn new() -> VecHeap {
        VecHeap {
            byte_size: 0,
            obj_incr: 0,
            obj_table: HashMap::new(),
            data: Vec::new(),
        }
    }
}

impl VecHeap {
    fn get_object_ind(&self, heap_i: HeapIndex) -> Option<ObjectIndex> {
        self.obj_table.iter().find_map(|(k, v)| {
            if *v as usize == heap_i as usize {
                Some(*k)
            } else {
                None
            }
        })
    }

    fn ro_size(&self, ro: &RuntimeObject) -> usize {
        let enum_size = size_of::<RuntimeObject>();
        match ro {
            //size of enum + size of vector
            RuntimeObject::Array(v) => size_of_val(&*v) + enum_size,
            //size of enum + size of both hashmaps
            RuntimeObject::Object {
                fields, methods, ..
            } => size_of_val(&fields) + size_of_val(&methods) + enum_size,
            _ => enum_size,
        }
    }
}

impl Heap for VecHeap {
    fn seek(&self, p: ObjectIndex) -> Result<RuntimeObject> {
        let heap_i = self
            .obj_table
            .get(&p)
            .ok_or(anyhow!("index not found {:?}", p))?;
        match self.data.get(*heap_i as usize) {
            Some(ho) => Ok(ho.ro.clone()),
            None => Err(anyhow!("internal error: undefined place on the (seek)")),
        }
    }

    fn seek_mut(&mut self, p: ObjectIndex) -> Result<&mut RuntimeObject> {
        let heap_i = self
            .obj_table
            .get(&p)
            .ok_or(anyhow!("index not found {:?}", p))?;
        match self.data.get_mut(*heap_i as usize) {
            Some(ho) => Ok(&mut ho.ro),
            None => Err(anyhow!(
                "internal error: undefined place on the heap (seek_mut)"
            )),
        }
    }

    fn alloc(&mut self, ro: RuntimeObject) -> ObjectIndex {
        match &ro {
            RuntimeObject::Array(_) | RuntimeObject::Object { .. } => {
                let obj_i = ObjectIndex(self.obj_incr);
                self.obj_table.insert(obj_i, self.data.len() as u64);
                self.byte_size += self.ro_size(&ro);
                self.data.push(HeapObj::new(ro));
                self.obj_incr += 1;
                return obj_i;
            }
            _ => (),
        };
        match self
            .data
            .iter()
            .enumerate()
            .find(|(_, d)| match (&d.ro, &ro) {
                (RuntimeObject::Bool(b1), RuntimeObject::Bool(b2)) => *b1 == *b2,
                (RuntimeObject::Integer(i1), RuntimeObject::Integer(i2)) => *i1 == *i2,
                (RuntimeObject::Null, RuntimeObject::Null) => true,
                _ => false,
            }) {
            Some((i, _)) => {
                let obj_i = self
                    .obj_table
                    .iter()
                    .find_map(|(k, v)| if *v as usize == i { Some(k) } else { None })
                    .unwrap();
                *obj_i
            }
            None => {
                let obj_i = ObjectIndex(self.obj_incr);
                self.obj_table.insert(obj_i, self.data.len() as HeapIndex);
                self.byte_size += self.ro_size(&ro);
                self.data.push(HeapObj::new(ro));
                self.obj_incr += 1;
                obj_i
            }
        }
    }

    fn byte_size(&self) -> usize {
        //we need to also calculate sizes of underlying vectors and hashmaps
        //(enums store only reference to them, so they do not count towards total size of enum)
        self.byte_size
    }

    fn mark(&mut self, ind: ObjectIndex) -> Result<()> {
        let heap_i = self
            .obj_table
            .get(&ind)
            .ok_or(anyhow!("couldn't find heap_i"))?;
        match self.data.get_mut(*heap_i as usize) {
            Some(ho) if !ho.mark => {
                ho.mark = true;
                //recursive mark for array and object
                match ho.ro.clone() {
                    RuntimeObject::Array(v) => v.iter().try_for_each(|i| self.mark(*i)),
                    RuntimeObject::Object { parent, fields, .. } => {
                        self.mark(parent)?;
                        fields.iter().try_for_each(|i| self.mark(*i.1))
                    }
                    _ => Ok(()),
                }
            }
            Some(_) => Ok(()),
            None => Err(anyhow!(
                "internal error: undefined place on the heap (mark)"
            )),
        }
    }

    fn collect_garbage(&mut self) -> () {
        let mut garbage_i = 1;
        //index 0 is NULL and we never want to garbage-collect it
        for i in 1..self.data.len() {
            let ho = &self.data[i];
            if !ho.mark {
                self.byte_size -= self.ro_size(&ho.ro)
            }
            if ho.mark && garbage_i != i {
                let ro = &mut self.data[i];
                ro.mark = false;

                let garbage_oi = self
                    .get_object_ind(garbage_i as u64)
                    .ok_or(anyhow!("garbage object index not found"))
                    .unwrap();
                let valid_oi = self
                    .get_object_ind(i as u64)
                    .ok_or(anyhow!("valid object index not found"))
                    .unwrap();

                self.data.swap(garbage_i, i);
                self.obj_table.insert(valid_oi, garbage_i as u64);
                self.obj_table.insert(garbage_oi, i as u64);
                garbage_i += 1
            } else if ho.mark {
                garbage_i += 1;
                self.data[i].mark = false
            }
        }
        for i in garbage_i..self.data.len() {
            match self
                .obj_table
                .iter()
                .find_map(|(k, v)| if *v as usize == i { Some(*k) } else { None })
            {
                Some(v) => self.obj_table.remove(&v).unwrap(),
                None => 0,
            };
        }
        self.data.truncate(garbage_i as usize);
        self.data.shrink_to_fit()
    }
}

pub trait Heap {
    fn seek(&self, p: ObjectIndex) -> Result<RuntimeObject>;
    fn seek_mut(&mut self, p: ObjectIndex) -> Result<&mut RuntimeObject>;
    fn alloc(&mut self, ro: RuntimeObject) -> ObjectIndex;
    fn byte_size(&self) -> usize;
    fn mark(&mut self, ind: ObjectIndex) -> Result<()>;
    fn collect_garbage(&mut self) -> ();
}
