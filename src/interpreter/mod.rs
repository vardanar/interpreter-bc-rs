mod frame;
mod heap;
mod logger;
mod stack;

use self::{
    heap::{Heap, RuntimeObject, VecHeap},
    stack::{Stack, VecStack},
};
use crate::interpreter::{frame::Frame, heap::NULL};
use crate::interpreter::{
    heap::ObjectIndex,
    logger::{EventType, Logger},
};
use crate::serializer::{
    instruction::{Index, Instruction},
    program_object::ProgramObject,
    Program,
};
use anyhow::*;
use std::{
    collections::HashMap,
    convert::{TryFrom, TryInto},
    io::Write,
};

pub struct Machine<W: Write, S: Stack = VecStack, H: Heap = VecHeap> {
    program: Program,
    out: W,
    stack: S,
    heap: H,
    ip: Index,
    global_frame: Frame,
    frame_stack: Vec<Frame>,
    logger: Logger,
    max_heap_size: usize,
}

impl<W: Write> Machine<W> {
    pub fn new(
        program: Program,
        out: W,
        max_heap_size_mb: u16,
        log_file: Option<String>,
    ) -> Machine<W> {
        let ip = program.entry;
        let mut heap = VecHeap::new();
        heap.alloc(RuntimeObject::Null);
        let global_frame = Frame::new(program.globals_lookup.len(), 0);
        let frame_stack = Vec::new();
        let logger = Logger::new(log_file);
        //convert MBs to bytes
        let max_heap_size = max_heap_size_mb as usize * 1024 * 1024;
        Machine {
            program,
            out,
            stack: VecStack::new(),
            heap,
            ip,
            global_frame,
            frame_stack,
            max_heap_size,
            logger,
        }
    }

    pub fn run(&mut self) -> Result<()> {
        let method = self.program.get_constant(self.program.entry)?.clone();
        match method {
            ProgramObject::Method { name, args, .. } => {
                self.handle_op(&Instruction::CallFunction { index: name, args })
            }
            _ => Err(anyhow!(
                "expected method as an entry point of the program, got something else"
            )),
        }?;
        let instr = self.program.instructions.clone();
        while self.ip as usize != instr.len() {
            self.handle_op(&instr[self.ip as usize])?;
        }
        self.logger.end()?;
        Ok(())
    }

    fn handle_op(&mut self, code: &Instruction) -> Result<()> {
        use crate::interpreter::Instruction::*;

        match *code {
            Literal(ind) => {
                let po = self.program.get_constant(ind)?;
                let ro = po.clone().try_into()?;
                self.alloc_push(ro)?;
                self.ip += 1;
                Ok(())
            }
            Drop => {
                self.stack.pop()?;
                self.ip += 1;
                Ok(())
            }
            Print { index, args } => {
                let cnst = self.program.get_constant(index)?.clone();
                if let ProgramObject::Str(s) = cnst {
                    self.print(&s, args)?;
                    self.ip += 1;
                    Ok(self.stack.push(NULL))
                } else {
                    Err(anyhow!("expected string in the constant pool"))
                }
            }
            Label(_) => {
                self.ip += 1;
                Ok(())
            }
            Jump(ind) => self.jump_to_lbl(ind),
            Branch(ind) => match self.pop_seek()? {
                RuntimeObject::Null | RuntimeObject::Bool(false) => {
                    self.ip += 1;
                    Ok(())
                }
                _ => self.jump_to_lbl(ind),
            },
            SetGlobal(ind) => {
                let loc_index = self.program.get_global_ind(ind)?;
                let obj_i = self.stack.top()?;
                self.global_frame.set(loc_index, obj_i)?;
                self.ip += 1;
                Ok(())
            }
            GetGlobal(ind) => {
                let loc_index = self.program.get_global_ind(ind)?;
                let var = self.global_frame.get(loc_index)?;
                self.stack.push(var);
                self.ip += 1;
                Ok(())
            }
            CallFunction {
                index,
                args: param_n,
            } => {
                let pool_index = self.program.get_global_ind(index)?;
                let method = self.program.get_constant(pool_index)?.clone();
                match method {
                    ProgramObject::Method {
                        args,
                        loc_n,
                        start: start_ip,
                        ..
                    } => {
                        if args != param_n {
                            return Err(anyhow!("incorrect number of method arguments"));
                        }
                        let frame_size = args as usize + loc_n as usize;
                        let mut frame = Frame::new(frame_size, self.ip + 1);
                        let fun_args: Vec<ObjectIndex> =
                            (0..args).map(|_| self.stack.pop()).collect::<Result<_>>()?;

                        fun_args
                            .iter()
                            .rev()
                            .enumerate()
                            .try_for_each(|(ind, v)| frame.set(ind as u16, *v))?;

                        self.frame_stack.push(frame);
                        self.ip = start_ip as u16;
                        Ok(())
                    }
                    v => Err(anyhow!("expected method got {:?}", v)),
                }
            }
            Return => {
                if self.frame_stack.len() == 0 {
                    Err(anyhow!("internal error: invalid frame stack pop"))
                } else {
                    let fs = self.frame_stack.pop().unwrap();
                    self.ip = fs.ret;
                    Ok(())
                }
            }
            GetLocal(ind) => {
                let ro_ind = self.frame_stack.last().map(|fs| fs.get(ind)).unwrap()?;
                self.stack.push(ro_ind);
                self.ip += 1;
                Ok(())
            }
            SetLocal(ind) => {
                let ro_ind = self.stack.top()?;
                self.frame_stack
                    .last_mut()
                    .map(|fs| fs.set(ind, ro_ind))
                    .unwrap()?;
                self.ip += 1;
                Ok(())
            }
            Array => {
                let init_val = self.pop_seek()?;
                let size = match self.pop_seek()? {
                    RuntimeObject::Integer(i) if i >= 0 => Ok(i as usize),
                    RuntimeObject::Integer(_) => {
                        Err(anyhow!("size of array can't be a negative number"))
                    }
                    _ => Err(anyhow!(
                        "size of array should be integer, got something else instead"
                    )),
                }?;
                let mut v = vec![NULL; size];
                for i in 0..size {
                    let ind = self.alloc(init_val.clone());
                    v[i] = ind;
                }
                self.alloc_push(RuntimeObject::Array(v))?;
                self.ip += 1;
                Ok(())
            }
            Object(ind) => {
                let po = self.program.get_constant(ind)?.clone();
                if let ProgramObject::Class(inds) = po {
                    let (fields, methods) = inds
                        .iter()
                        .rev()
                        .try_fold((HashMap::new(), HashMap::new()), |(mut fs, mut ms), i| {
                        let po = self.program.get_constant(*i)?;
                            match po {
                                ProgramObject::Slot(i) => {
                                    if fs.contains_key(i) {
                                        Err(anyhow!("field already defined in object"))
                                    } else {
                                        let field_v = self.stack.pop()?;
                                        fs.insert(*i, field_v);
                                        Ok((fs, ms))
                                    }
                                }
                                ProgramObject::Method { name, .. } => {
                                    if ms.contains_key(name) {
                                        Err(anyhow!("method already defined in object"))
                                    } else {
                                        ms.insert(*name, *i);
                                        Ok((fs, ms))
                                    }
                                }
                                _ => Err(anyhow!("object can contain only fields and methods, found {:?} instead", po)),
                            }
                    })?;
                    let parent = self.stack.pop()?;
                    self.alloc_push(RuntimeObject::Object {
                        fields,
                        methods,
                        parent,
                    })?;
                    self.ip += 1;
                    Ok(())
                } else {
                    Err(anyhow!("expected program object of type class got {?:}",))
                }
            }
            CallMethod {
                index,
                args: args_n,
            } => {
                let mut args = (0..(args_n - 1))
                    .map(|_| {
                        let i = self.stack.pop()?;
                        let ro = self.heap.seek(i)?;
                        Ok((i, ro))
                    })
                    .collect::<Result<Vec<(ObjectIndex, RuntimeObject)>>>()?;
                args.reverse();
                //get_constant is immutable borrow of self, so clone is necessary, because we borrow self mutably later on
                let method_name_obj = self.program.get_constant(index).map(Clone::clone)?;
                let caller_ind = self.stack.pop()?;
                let method_name = match method_name_obj {
                    ProgramObject::Str(s) => Ok(s),
                    _ => Err(anyhow!(
                        "expected string method name, got something else instead"
                    )),
                }?;
                self.method_dispatch(caller_ind, index, method_name.as_str(), args)?;
                Ok(())
            }
            GetField(index) => match self.pop_seek()? {
                RuntimeObject::Object { fields, .. } => match fields.get(&index) {
                    Some(field_ind) => {
                        self.stack.push(*field_ind);
                        self.ip += 1;
                        Ok(())
                    }
                    None => {
                        fields.iter().for_each(|(f, _)| {
                            println!(
                                "object field: {:?}",
                                self.program.get_constant(*f as u16).unwrap()
                            )
                        });
                        Err(anyhow!(
                            "undefined object field, {:?}",
                            self.program.get_constant(index as u16).unwrap()
                        ))
                    }
                },
                v => Err(anyhow!(
                    "expected object as a get field receiver, got {:?}",
                    v
                )),
            },
            SetField(index) => {
                let val_ind = self.stack.pop()?;
                match self.pop_seek_mut()? {
                    RuntimeObject::Object { ref mut fields, .. } => {
                        if !fields.contains_key(&index) {
                            return Err(anyhow!("undefined object field"));
                        }
                        fields.insert(index, val_ind);
                        self.stack.push(val_ind);
                        self.ip += 1;
                        Ok(())
                    }
                    _ => Err(anyhow!(
                        "expected object as a set field receiver, got something else instead"
                    )),
                }
            }
        }
    }

    fn print(&mut self, format: &str, args_n: u8) -> Result<()> {
        let mut args: Vec<RuntimeObject> = (0..args_n)
            .map(|_| self.pop_seek())
            .collect::<Result<_>>()?;
        format
            .chars()
            .try_fold((false, 0), |acc, x| match (x, acc.0) {
                ('~', true) => {
                    write!(self.out, "~")?;
                    Ok((false, acc.1))
                }
                ('~', false) => {
                    let ro = args
                        .pop()
                        .ok_or(anyhow!("internal error: not enough arguments to print"))?;
                    write!(self.out, "{}", self.runtime_to_string(ro)?)?;
                    Ok((false, acc.1 + 1))
                }
                ('n', true) => {
                    write!(self.out, "\n")?;
                    Ok((false, acc.1))
                }
                ('t', true) => {
                    write!(self.out, "\t")?;
                    Ok((false, acc.1))
                }
                ('r', true) => {
                    write!(self.out, "\r")?;
                    Ok((false, acc.1))
                }
                ('"', true) => {
                    write!(self.out, "\"")?;
                    Ok((false, acc.1))
                }
                ('\\', false) => Ok((true, acc.1)),
                ('\\', true) => {
                    write!(self.out, "\\")?;
                    Ok((false, acc.1))
                }
                (_, true) => Err(anyhow!("Unknown escape sequence in print")),
                _ => {
                    write!(self.out, "{}", x)?;
                    Ok((false, acc.1))
                }
            })?;
        Ok(())
    }

    fn garbage_collect(&mut self) -> Result<()> {
        if self.heap.byte_size() > self.max_heap_size {
            self.mark()?;
            self.heap.collect_garbage();
            self.logger.log(EventType::Gc, self.heap.byte_size());
        };
        Ok(())
    }

    fn alloc(&mut self, ro: RuntimeObject) -> ObjectIndex {
        let heap_ind = self.heap.alloc(ro);
        self.logger.log(EventType::Alloc, self.heap.byte_size());
        heap_ind
    }

    fn alloc_push(&mut self, ro: RuntimeObject) -> Result<ObjectIndex> {
        let p = self.alloc(ro);
        self.stack.push(p);
        self.garbage_collect()?;
        Ok(p)
    }

    fn pop_seek(&mut self) -> Result<RuntimeObject> {
        self.stack.pop().map(|op| self.heap.seek(op))?
    }

    fn pop_seek_mut(&mut self) -> Result<&mut RuntimeObject, Error> {
        let ind = self.stack.pop()?;
        self.heap.seek_mut(ind)
    }

    fn jump_to_lbl(&mut self, ind: Index) -> Result<()> {
        match self.program.labels.get(&ind) {
            Some(lbl) => {
                self.ip = *lbl;
                Ok(())
            }
            None => Err(anyhow!("undefined label")),
        }
    }

    fn runtime_to_string(&self, ro: RuntimeObject) -> Result<String> {
        match ro {
            RuntimeObject::Array(v) => Ok(format!(
                "[{}]",
                v.iter()
                    .map(|ind| self.heap.seek(*ind))
                    .map(|ro| self.runtime_to_string(ro?))
                    .collect::<Result<Vec<String>>>()?
                    .join(",")
            )),
            RuntimeObject::Object { parent, fields, .. } => {
                let mut fs = fields
                    .iter()
                    .map(|(const_ind, heap_ind)| {
                        let name = match self.program.get_constant(*const_ind)? {
                            ProgramObject::Str(s) => Ok(s),
                            _ => Err(anyhow!("expected field name, got something else instead")),
                        }?;
                        let value = self.heap.seek(*heap_ind)?;
                        Ok(format!("{}={}", name, self.runtime_to_string(value)?))
                    })
                    .collect::<Result<Vec<String>>>()?;
                //not necessary, but we need some deterministic order for testing (HashMap order is not deterministic)
                fs.sort();
                let p = self.heap.seek(parent)?;
                match p {
                    RuntimeObject::Null => Ok(format!("object({})", fs.join(", "))),
                    _ if fields.is_empty() => {
                        Ok(format!("object(..={})", self.runtime_to_string(p)?))
                    }
                    _ => Ok(format!(
                        "object(..={}, {})",
                        self.runtime_to_string(p)?,
                        fs.join(", ")
                    )),
                }
            }
            RuntimeObject::Bool(b) => Ok(b.to_string()),
            RuntimeObject::Integer(i) => Ok(i.to_string()),
            RuntimeObject::Null => Ok("null".to_string()),
        }
    }

    fn method_dispatch(
        &mut self,
        caller_ind: ObjectIndex,
        method_name_ind: u16,
        method_name: &str,
        //first = index to heap where RO is stored, second = RO itself
        mut args: Vec<(ObjectIndex, RuntimeObject)>,
    ) -> Result<()> {
        use RuntimeObject::*;
        let caller = self.heap.seek_mut(caller_ind)?;
        match caller {
            RuntimeObject::Null if args.len() == 1 => {
                let b = match (method_name, &args[0].1) {
                    ("eq", Null) | ("==", Null) => true,
                    ("eq", _) | ("==", _) => false,
                    ("neq", Null) | ("!=", Null) => false,
                    ("neq", _) | ("!=", _) => true,
                    _ => Err(anyhow!("unknown object method {}", method_name))?,
                };
                self.alloc_push(RuntimeObject::Bool(b))?;
                self.ip += 1;
                Ok(())
            }
            RuntimeObject::Array(ref mut v) => {
                match method_name {
                    "get" if args.len() == 1 => match args[0].1 {
                        Integer(i) if (i as usize) < v.len() => {
                            self.stack.push(v[i as usize]);
                            Ok(())
                        }
                        Integer(i) => Err(anyhow!("array out of bounds access at index {}", i)),
                        _ => Err(anyhow!(
                            "expected array index (integer), got something else"
                        )),
                    },
                    "set" if args.len() == 2 => match args[0].1 {
                        Integer(i) if (i as usize) < v.len() => {
                            v[i as usize] = args[1].0;
                            self.stack.push(args[1].0);
                            Ok(())
                        }
                        Integer(i) => Err(anyhow!("array out of bounds access at index {}", i)),
                        _ => Err(anyhow!(
                            "expected array index (integer), got {} instead",
                            self.runtime_to_string(args[0].1.clone())?
                        )),
                    },
                    _ => Err(anyhow!(
                        "unknown array method {} with {} args",
                        method_name,
                        args.len()
                    )),
                }?;
                self.ip += 1;
                Ok(())
            }
            RuntimeObject::Integer(v) => {
                let op1 = v.clone();
                if args.len() != 1 {
                    return Err(anyhow!(
                        "integer method {} takes only one argument",
                        method_name
                    ));
                }
                let op2 = args.get(0).unwrap();
                let ro = match op2.1 {
                    RuntimeObject::Integer(op2) => match method_name {
                        "+" | "add" => (op1 + op2).into(),
                        "-" | "sub" => (op1 - op2).into(),
                        "*" | "mul" => (op1 * op2).into(),
                        "/" | "div" => {
                            if op2 == 0 {
                                return Err(anyhow!("division by zero"));
                            } else {
                                (op1 / op2).into()
                            }
                        }
                        "%" | "mod" => (op1 % op2).into(),
                        "<=" | "le" => (op1 <= op2).into(),
                        ">=" | "ge" => (op1 >= op2).into(),
                        "<" | "lt" => (op1 < op2).into(),
                        ">" | "gt" => (op1 > op2).into(),
                        "==" | "eq" => (op1 == op2).into(),
                        "!=" | "neq" => (op1 != op2).into(),
                        v => Err(anyhow!("unknown integer method {}", v))?,
                    },
                    _ => match method_name {
                        "==" | "eq" => false.into(),
                        "!=" | "neq" => true.into(),
                        v => Err(anyhow!("unknown integer method {}", v))?,
                    },
                };
                self.alloc_push(ro)?;
                self.ip += 1;
                Ok(())
            }
            RuntimeObject::Bool(b) => {
                let op1 = b.clone();
                if args.len() != 1 {
                    return Err(anyhow!(
                        "bool method {} takes only one argument",
                        method_name
                    ));
                }
                let op2 = args.get(0).unwrap();
                let ro: RuntimeObject = match op2.1 {
                    Bool(op2) => match method_name {
                        "&" | "and" => (op1 && op2).into(),
                        "|" | "or" => (op1 || op2).into(),
                        "==" | "eq" => (op1 == op2).into(),
                        "!=" | "neq" => (op1 != op2).into(),
                        v => Err(anyhow!("unknown bool method {}", v))?,
                    },
                    _ => match method_name {
                        "==" | "eq" => false.into(),
                        "!=" | "neq" => true.into(),
                        v => Err(anyhow!("unknown bool method {}", v))?,
                    },
                };
                self.alloc_push(ro)?;
                self.ip += 1;
                Ok(())
            }
            RuntimeObject::Object {
                parent,
                fields: _,
                methods,
            } => {
                if methods.contains_key(&method_name_ind) {
                    let method_ind = methods.get(&method_name_ind).unwrap();
                    let po = self.program.get_constant(*method_ind).unwrap();
                    match po {
                        ProgramObject::Method {
                            name: _,
                            args: args_n,
                            loc_n,
                            ins_n: _,
                            start,
                        } => {
                            let start = *start;
                            let frame_size = *args_n as usize + *loc_n as usize + 1; // + 1 for 'this'
                            let mut frame = Frame::new(frame_size, self.ip + 1);
                            frame.set(0, caller_ind)?;
                            let fun_args: Vec<ObjectIndex> = (0..*args_n - 1)
                                .map(|_| {
                                    let s = args.pop().ok_or(anyhow!("argument mismatched"))?;
                                    Ok(s.0)
                                })
                                .collect::<Result<_>>()?;
                            fun_args
                                .iter()
                                .rev()
                                .enumerate()
                                .try_for_each(|(ind, v)| frame.set((ind + 1) as u16, *v))?;
                            self.frame_stack.push(frame);
                            self.ip = start as u16;
                            Ok(())
                        }
                        _ => Ok(()),
                    }
                } else if *parent != NULL {
                    let parent = parent.clone();
                    self.method_dispatch(parent, method_name_ind, method_name, args)
                } else {
                    Err(anyhow!(
                        "method {:?} is not defined for {:?}",
                        method_name,
                        {
                            let p = parent.clone();
                            self.heap.seek(p)?
                        }
                    ))
                }
            }
            v => Err(anyhow!(
                "unexpected caller {:?} method_name: {:?} args: {:?}",
                v,
                method_name,
                args
            )),
        }
    }

    fn mark(&mut self) -> Result<()> {
        //marking global frame
        self.global_frame
            .copy_all()
            .iter()
            .try_for_each(|i| self.heap.mark(*i))?;

        //marking all function frames
        let mut heap_ids = Vec::new();
        self.frame_stack
            .iter()
            .for_each(|f| heap_ids.append(&mut f.copy_all()));
        heap_ids
            .iter()
            .try_for_each(|i| self.heap.mark(i.clone()))?;

        //marking stack
        self.stack
            .copy_all()
            .iter()
            .try_for_each(|i| self.heap.mark(i.clone()))
    }
}

impl TryFrom<ProgramObject> for RuntimeObject {
    type Error = anyhow::Error;

    fn try_from(po: ProgramObject) -> Result<Self, Self::Error> {
        match po {
            ProgramObject::Integer(v) => Ok(v.into()),
            ProgramObject::Null => Ok(RuntimeObject::Null),
            ProgramObject::Bool(b) => Ok(b.into()),
            _ => Err(anyhow!("expected Integer, Null or Boolean")),
        }
    }
}
