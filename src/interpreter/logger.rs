use anyhow::*;
use std::fmt::Write as FmtWrite;
use std::fs;
use std::time::SystemTime;

pub enum EventType {
    Start,
    Alloc,
    Gc,
}

pub struct Event {
    e_type: EventType,
    timestamp: u64,
    bytes: usize,
}

impl Event {
    pub fn new(e_type: EventType, bytes: usize) -> Event {
        let timestamp = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        Event {
            e_type,
            timestamp,
            bytes,
        }
    }

    pub fn to_string(&self) -> String {
        format!(
            "{},{},{}",
            self.timestamp,
            match self.e_type {
                EventType::Start => "S",
                EventType::Alloc => "A",
                EventType::Gc => "G",
            },
            self.bytes
        )
    }
}

pub struct Logger {
    log_file: Option<String>,
    events: Vec<Event>,
}

impl Logger {
    pub fn new(log_file: Option<String>) -> Logger {
        let mut events = Vec::new();
        events.push(Event::new(EventType::Start, 0));
        Logger { log_file, events }
    }

    pub fn log(&mut self, e_type: EventType, bytes: usize) {
        self.events.push(Event::new(e_type, bytes))
    }

    pub fn end(&self) -> Result<()> {
        match self.log_file.as_ref() {
            Some(filename) => {
                let mut out = String::new();
                writeln!(&mut out, "timestamp,event,heap")?;
                self.events
                    .iter()
                    .try_for_each(|e| writeln!(&mut out, "{}", e.to_string()))?;
                fs::write(filename, out).expect("Unable to write a log csv");
                Ok(())
            }
            None => Ok(()),
        }
    }
}
