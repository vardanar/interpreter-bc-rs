use anyhow::{anyhow, Result};

use super::heap::ObjectIndex;

pub trait Stack {
    fn push(&mut self, p: ObjectIndex);

    fn pop(&mut self) -> Result<ObjectIndex>;

    fn top(&self) -> Result<ObjectIndex>;

    fn copy_all(&self) -> Vec<ObjectIndex>;
}

#[derive(Debug)]
pub struct VecStack {
    data: Vec<ObjectIndex>,
}

impl VecStack {
    pub fn new() -> VecStack {
        VecStack { data: Vec::new() }
    }
}

impl Stack for VecStack {
    fn push(&mut self, p: ObjectIndex) {
        self.data.push(p)
    }

    fn pop(&mut self) -> Result<ObjectIndex> {
        self.data
            .pop()
            .ok_or(anyhow!("internal error: empty top of the stack"))
    }

    fn top(&self) -> Result<ObjectIndex> {
        self.data
            .last()
            .map(ObjectIndex::clone)
            .ok_or(anyhow!("internal error: empty top of the stack"))
    }

    fn copy_all(&self) -> Vec<ObjectIndex> {
        self.data.clone()
    }
}
