use crate::{interpreter::heap::NULL, serializer::instruction::Index};
use anyhow::{anyhow, Result};

use super::heap::ObjectIndex;

#[derive(Debug)]
pub struct Frame {
    pub locals: Vec<ObjectIndex>,
    pub ret: Index,
}

impl Frame {
    pub fn new(size: usize, ret: Index) -> Frame {
        Frame {
            //0 = RuntimeObject::Null
            locals: vec![NULL; size],
            ret,
        }
    }

    pub fn get(&self, ind: Index) -> Result<ObjectIndex> {
        let i = ind as usize;
        if i < self.locals.len() {
            Ok(self.locals[i])
        } else {
            Err(anyhow!(
                "undefined frame local index {} max: {}",
                ind,
                self.locals.len()
            ))
        }
    }

    pub fn set(&mut self, ind: Index, heap_ind: ObjectIndex) -> Result<()> {
        let i = ind as usize;
        if i < self.locals.len() {
            self.locals[i] = heap_ind;
            Ok(())
        } else {
            Err(anyhow!("undefined frame local index"))
        }
    }

    pub fn copy_all(&self) -> Vec<ObjectIndex> {
        self.locals.clone()
    }
}
