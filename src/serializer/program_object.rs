use crate::serializer::instruction::Instruction;
use crate::serializer::native::LeRead;
use crate::serializer::program_object::ProgramObject::Method;
use anyhow::{anyhow, Result};
use std::io::Read;
use std::str;

type Index = u16;

#[derive(Clone, Debug)]
pub enum ProgramObject {
    Integer(i32),
    Bool(bool),
    Null,
    Str(String),
    Slot(Index),
    Method {
        name: Index,
        args: u8,
        loc_n: u16,
        ins_n: u32,
        start: u32,
    },
    Class(Vec<Index>),
}

impl ProgramObject {
    #[inline]
    pub fn from_bytes<R: Read>(
        mut input: &mut R,
        instructions: &mut Vec<Instruction>,
    ) -> Result<ProgramObject> {
        let tag = u8::le_read(&mut input)?;
        match tag {
            0x00 => Ok(ProgramObject::Integer(i32::le_read(&mut input)?)),
            0x01 => Ok(ProgramObject::Null),
            0x02 => {
                let size = u32::le_read(&mut input)? as usize;
                let mut buf = vec![0; size];
                for i in 0..size {
                    buf[i] = u8::le_read(&mut input)?;
                }
                Ok(ProgramObject::Str(
                    str::from_utf8(buf.as_slice())?.to_string(),
                ))
            }
            0x03 => {
                let name = u16::le_read(&mut input)?;
                let args = u8::le_read(&mut input)?;
                let loc_n = u16::le_read(&mut input)?;
                let ins_n = u32::le_read(&mut input)?;
                let start = instructions.len() as u32;
                for _i in 0..ins_n {
                    instructions.push(Instruction::from_bytes(&mut input)?);
                }
                Ok(Method {
                    name,
                    args,
                    loc_n,
                    ins_n,
                    start,
                })
            }
            0x04 => Ok(ProgramObject::Slot(u16::le_read(&mut input)?)),
            0x05 => {
                let size = u16::le_read(&mut input)? as usize;
                let mut members = vec![0; size];
                for i in 0..size {
                    members[i] = u16::le_read(&mut input)?;
                }
                Ok(ProgramObject::Class(members))
            }
            0x06 => Ok(ProgramObject::Bool(match u8::le_read(&mut input)? {
                0x00 => false,
                _ => true,
            })),
            _ => Err(anyhow!("Unknown program object")),
        }
    }
}
