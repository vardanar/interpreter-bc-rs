pub mod instruction;
pub mod native;
pub mod program_object;

use crate::serializer::instruction::Instruction;
use crate::serializer::native::LeRead;
use crate::serializer::program_object::ProgramObject;
use crate::serializer::program_object::ProgramObject::Method;
use anyhow::*;
use std::collections::HashMap;
use std::io::{BufReader, Read};

type Index = u16;

#[derive(Clone)]
pub struct Program {
    pub const_pool: Vec<ProgramObject>,
    pub globals: Vec<Index>,
    pub entry: Index,
    pub instructions: Vec<Instruction>,
    //key: label, val: location of label
    pub labels: HashMap<Index, Index>,
    //key: const pool index, val: Frame locals index
    pub globals_lookup: HashMap<Index, Index>,
}

impl Program {
    pub fn from_bc<R: Read>(input: &mut R) -> Result<Program> {
        let mut input = BufReader::new(input);
        //globals lookup table - translates const pool index to Frame locals index or Method index
        let mut globals_lookup: HashMap<Index, Index> = HashMap::new();

        //const pool
        let mut instructions: Vec<Instruction> = Vec::new();
        let const_size = u16::le_read(&mut input)? as usize;
        let mut const_pool = vec![ProgramObject::Null; const_size];
        for i in 0..const_size {
            const_pool[i] = ProgramObject::from_bytes(&mut input, &mut instructions)?;
            //if it's method, we will add it to globals_lookup
            match const_pool[i] {
                Method { name, .. } => {
                    globals_lookup.insert(name, i as u16);
                    ()
                }
                _ => (),
            };
        }

        //globals
        let globals_size = u16::le_read(&mut input)? as usize;
        let mut globals: Vec<Index> = vec![0; globals_size];
        for i in 0..globals_size {
            globals[i] = u16::le_read(&mut input)?;
            //check if global points to correct ProgramObject
            match const_pool[globals[i] as usize] {
                ProgramObject::Slot(const_index) => {
                    let loc_index = globals_lookup.len() as Index;
                    globals_lookup.insert(const_index, loc_index);
                    Ok(())
                }
                ProgramObject::Method { .. } => Ok(()),
                _ => Err(anyhow!("global index can point only to Slot or Method")),
            }?
        }

        //entry point
        let entry = u16::le_read(&mut input)?;

        //label table
        let mut labels: HashMap<Index, Index> = HashMap::new();
        //filling label table and checking that variables and functions are represented by string
        for i in 0..instructions.len() {
            match instructions[i] {
                Instruction::Label(ind) => match &const_pool[ind as usize] {
                    ProgramObject::Str(_) if !labels.contains_key(&ind) => {
                        labels.insert(ind, i as u16);
                        Ok(())
                    }
                    ProgramObject::Str(name) => Err(anyhow!("label {} already defined", name)),
                    _ => Err(anyhow!("internal error: expected label name, got something that is definitely not a label name"))
                },
                Instruction::GetField(ind) | Instruction::SetField(ind) => match &const_pool[ind as usize] {
                    ProgramObject::Str(_) => Ok(()),
                    _ => Err(anyhow!("internal error: expected field name, got something that is definitely not a field name"))
                }
                Instruction::GetGlobal(ind) | Instruction::SetGlobal(ind) => match &const_pool[ind as usize] {
                    ProgramObject::Str(_) => Ok(()),
                    _ => Err(anyhow!("internal error: expected variable name, got something that is definitely not a variable name"))
                }
                Instruction::CallFunction { index, .. } => match &const_pool[index as usize] {
                    ProgramObject::Str(_) => Ok(()),
                    _ => Err(anyhow!("internal error: expected function name, got something that is definitely not a function name"))
                },
                Instruction::CallMethod { index, .. } => match &const_pool[index as usize] {
                    ProgramObject::Str(_) => Ok(()),
                    _ => Err(anyhow!("internal error: expected method name, got something that is definitely not a method name"))
                }
                _ => Ok(()),
            }?;
        }

        //checking that every Slot and Method has correct string name
        const_pool.iter().try_for_each(|x| match x {
            ProgramObject::Slot(ind) => match &const_pool[*ind as usize] {
                ProgramObject::Str(_) => Ok(()),
                _ => Err(anyhow!("internal error: expected variable name, got something that is definitely not a variable name"))
            },
            ProgramObject::Method { name, .. } => match &const_pool[*name as usize] {
                ProgramObject::Str(_) => Ok(()),
                _ => Err(anyhow!("internal error: expected method name, got something that is definitely not a method name"))
            },
            _ => Ok(()),
        })?;
        /*
        println!("Const pool:");
        const_pool.iter().for_each(|i| println!("{:?}", i));
        println!();

        println!("Globals:");
        globals.iter().for_each(|i| println!("{:?}", i));
        println!();

        println!("Entry: {}", entry);
        println!();

        println!("Instructions:");
        instructions.iter().for_each(|i| println!("{:?}", i));*/

        Ok(Program {
            const_pool,
            globals,
            entry,
            instructions,
            labels,
            globals_lookup,
        })
    }

    pub fn get_global_ind(&self, const_ind: Index) -> Result<Index> {
        match self.globals_lookup.get(&const_ind) {
            Some(x) => Ok(*x),
            None => Err(anyhow!("undefined global variable access {}", const_ind)),
        }
    }

    pub fn get_constant(&self, ind: Index) -> Result<&ProgramObject> {
        self.const_pool
            .get(ind as usize)
            .ok_or(anyhow!("unknown index {} to constant pool"))
    }
}
