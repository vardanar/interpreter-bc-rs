use anyhow::Result;
use std::{io::Read, mem};

pub trait LeRead: Sized {
    fn le_read<R: Read>(input: &mut R) -> Result<Self>;
}

macro_rules! impl_le_read {
    ($type:ty) => {
        impl LeRead for $type {
            fn le_read<R: Read>(input: &mut R) -> Result<Self> {
                let mut buf = [0u8; mem::size_of::<Self>()];
                input.read_exact(&mut buf)?;
                Ok(Self::from_le_bytes(buf))
            }
        }
    };
}

impl_le_read!(u16);
impl_le_read!(u8);
impl_le_read!(u32);
impl_le_read!(i32);
