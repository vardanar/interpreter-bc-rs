use crate::serializer::native::LeRead;
use anyhow::{anyhow, Result};
use std::io::Read;

pub type Index = u16;

#[derive(Clone, Debug)]
pub enum Instruction {
    Label(Index),
    Literal(Index),
    Print { index: Index, args: u8 },
    Array,
    Object(Index),
    GetField(Index),
    SetField(Index),
    CallMethod { index: Index, args: u8 },
    CallFunction { index: Index, args: u8 },
    SetLocal(Index),
    GetLocal(Index),
    SetGlobal(Index),
    GetGlobal(Index),
    Branch(Index),
    Jump(Index),
    Return,
    Drop,
}

impl Instruction {
    #[inline]
    pub fn from_bytes<R: Read>(mut input: &mut R) -> Result<Instruction> {
        let opcode = u8::le_read(&mut input)?;
        match opcode {
            0x00 => Ok(Instruction::Label(u16::le_read(&mut input)?)),
            0x01 => Ok(Instruction::Literal(u16::le_read(&mut input)?)),
            0x02 => {
                let index = u16::le_read(&mut input)?;
                let args = u8::le_read(&mut input)?;
                Ok(Instruction::Print { index, args })
            }
            0x03 => Ok(Instruction::Array),
            0x04 => Ok(Instruction::Object(u16::le_read(&mut input)?)),
            0x05 => Ok(Instruction::GetField(u16::le_read(&mut input)?)),
            0x06 => Ok(Instruction::SetField(u16::le_read(&mut input)?)),
            0x07 => {
                let index = u16::le_read(&mut input)?;
                let args = u8::le_read(&mut input)?;
                Ok(Instruction::CallMethod { index, args })
            }
            0x08 => {
                let index = u16::le_read(&mut input)?;
                let args = u8::le_read(&mut input)?;
                Ok(Instruction::CallFunction { index, args })
            }
            0x09 => Ok(Instruction::SetLocal(u16::le_read(&mut input)?)),
            0x0A => Ok(Instruction::GetLocal(u16::le_read(&mut input)?)),
            0x0B => Ok(Instruction::SetGlobal(u16::le_read(&mut input)?)),
            0x0C => Ok(Instruction::GetGlobal(u16::le_read(&mut input)?)),
            0x0D => Ok(Instruction::Branch(u16::le_read(&mut input)?)),
            0x0E => Ok(Instruction::Jump(u16::le_read(&mut input)?)),
            0x0F => Ok(Instruction::Return),
            0x10 => Ok(Instruction::Drop),
            _ => Err(anyhow!("Unknown instruction")),
        }
    }
}
